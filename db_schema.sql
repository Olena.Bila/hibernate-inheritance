create table author (
    id int not null auto_increment,
    first_name varchar(100),
    last_name varchar(100),
    constraint pk_book primary key (id)
);

create table book (
  id int not null auto_increment,
  title varchar(40),
  publishing_date date,
  isbn varchar(15),
  number_of_pages int,
  constraint pk_book primary key (id)
);

create table blog_post (
  id int not null auto_increment,
  title varchar(100),
  publishing_date date,
  url varchar(200),
  constraint pk_book primary key (id)
);

create table author_publication (
  author_id int not null,
  publication_id int not null,
  constraint pk_author_publication primary key (author_id, publication_id),
  constraint fk_author_id foreign key (author_id) references author(id) on delete cascade
);

create table id_publication (
    pk varchar(255),
    value int
);

--single class strategy
create table publication (
  id int not null auto_increment,
  title varchar(40),
  publishing_date date,
  isbn varchar(15),
  number_of_pages int,
  url varchar(200),
  publication_type varchar(2) not null,
  constraint pk_book primary key (id)
);

alter table author_publication add constraint fk_publication_id foreign key (publication_id) references publication (id)
 on delete cascade;

--join strategy
create table publication_join (
  id int not null auto_increment,
  title varchar(40),
  publishing_date date,
  constraint pk_book primary key (id)
);

create table book_join (
  id int not null,
  isbn varchar(15),
  number_of_pages int,
  constraint pk_book primary key (id),
  constraint fk_publication foreign key (id) references publication_join (id)
);

create table blog_post_join (
  id int not null,
  url varchar(200),
  constraint pk_book primary key (id),
  constraint fk_publication_bp foreign key (id) references publication_join (id)
);

alter table add constraint fk_publication_id foreign key (publication_id) references publication_join (id) on delete cascade;
