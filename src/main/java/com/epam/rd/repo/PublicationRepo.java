package com.epam.rd.repo;

import com.epam.rd.domain.Publication;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class PublicationRepo {
    private final String PERSISTENCE_UNIT_NAME = "publication_PU";

    private EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
    private EntityManager entityManager = factory.createEntityManager();

    public Publication find(Long id) {
        return entityManager.find(Publication.class, id);
    }

    @SuppressWarnings("unchecked")
    public List<Publication> findAll() {
        return entityManager.createQuery("select p from Publication").getResultList();
    }

    public void create(Publication product) {
        entityManager.getTransaction().begin();
        entityManager.persist(product);
        entityManager.getTransaction().commit();
    }

    public Publication update(Publication product) {
        entityManager.getTransaction().begin();
        Publication result = entityManager.merge(product);
        entityManager.getTransaction().commit();
        return result;
    }

    public void delete(Publication product) {
        entityManager.getTransaction().begin();
        entityManager.remove(product);
        entityManager.getTransaction().commit();
    }
}
