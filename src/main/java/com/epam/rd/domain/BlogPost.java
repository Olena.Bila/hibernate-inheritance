package com.epam.rd.domain;

public class BlogPost extends Publication {
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
